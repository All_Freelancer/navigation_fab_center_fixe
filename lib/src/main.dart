import 'package:flutter/material.dart';
import 'package:navigation_fab_app/src/ui/widgets/navigation.dart';

//******************************** MAIN APP ************************************
void main() => runApp(MyApp());


//****************************** WIDGET MAIN ***********************************
class MyApp extends StatelessWidget {

  //:::::::::::::::: This widget is the root of your application :::::::::::::::
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Navigation() ,
    );
  }
}
//******************************************************************************

