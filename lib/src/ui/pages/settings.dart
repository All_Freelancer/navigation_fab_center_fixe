import 'package:flutter/material.dart';

//****************************** CLASS SETTINGS ********************************
class Settings extends StatefulWidget {
  @override
  _SettingsState createState() => _SettingsState();
}

//******************************** STATE SETTINGS ******************************
class _SettingsState extends State<Settings> {

  //*************************** WIDGET ROOT SETTINGS ***************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Settings'),
      ),
    );
  }
}
//******************************************************************************