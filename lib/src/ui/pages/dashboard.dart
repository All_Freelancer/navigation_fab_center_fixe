import 'package:flutter/material.dart';

//****************************** CLASS DASHBOARD *******************************
class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

//******************************** STATE DASHBOARD *****************************
class _DashboardState extends State<Dashboard> {

  //*************************** WIDGET ROOT DASHBOARD **************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dashboard'),
      ),
    );
  }
}
//******************************************************************************