import 'package:flutter/material.dart';
import 'package:navigation_fab_app/src/ui/pages/chat.dart';
import 'package:navigation_fab_app/src/ui/pages/dashboard.dart';
import 'package:navigation_fab_app/src/ui/pages/profile.dart';
import 'package:navigation_fab_app/src/ui/pages/settings.dart';

//***************************** CLASS NAVIGATION *******************************
class Navigation extends StatefulWidget {
  @override
  _NavigationState createState() => _NavigationState();
}

//******************************* STATE NAVIGATION *****************************
class _NavigationState extends State<Navigation> {
  // Properties & Variables needed
  int currentTab = 0; // to keep track of active tab index
  final List<Widget> screens = [
    Dashboard(),
    Chat(),
    Profile(),
    Settings(),
  ]; // to store nested tabs

  final PageStorageBucket bucket = PageStorageBucket();
  Widget currentScreen = Dashboard(); // Our first view in viewport

  //*************************** WIDGET ROOT HOME *******************************
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //::::::::::::::::::::::::: PAGE STORAGE ::::::::::::::::::::::::::::
      body: PageStorage(
        child: currentScreen,         // FIRST FRAGMENT - SCREEN
        bucket: bucket,               //
      ),

      //::::::::::::::::::::::: FLOATING ACTION BUTTON :::::::::::::::::::::
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.add),
        onPressed: () {},
      ),

      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,

      //:::::::::::::::::::::::: NAVIGATION BAR ::::::::::::::::::::::::::::
      bottomNavigationBar: buildNavigation()

    );
  }

  //*************************** BUILD NAVIGATION BAR ***************************
  Widget buildNavigation(){
    return  BottomAppBar(
      //:::::::::::::::::::::::::::: SHAPE :::::::::::::::::::::::
      shape: CircularNotchedRectangle(),

      //:::::::::::::::::::::::::::: MARGIN ::::::::::::::::::::::
      notchMargin: 10,

      //::::::::::::::::::::::::: CONTAINER ::::::::::::::::::::::
      child: Container(
        height: 60,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            //-------------- ROW LEFT (TAB BAR ICON) -------------------
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //.................. CREATE BUTTON DASHBOARD ............
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentScreen =
                          Dashboard(); // if user taps on this dashboard tab will be active
                      currentTab = 0;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.dashboard,
                        color: currentTab == 0 ? Colors.blue : Colors.grey,
                      ),
                      Text(
                        'Dashboard',
                        style: TextStyle(
                          color: currentTab == 0 ? Colors.blue : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),

                //.................. CREATE BUTTON CHAT ............
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentScreen =
                          Chat(); // if user taps on this dashboard tab will be active
                      currentTab = 1;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.chat,
                        color: currentTab == 1 ? Colors.blue : Colors.grey,
                      ),
                      Text(
                        'Chats',
                        style: TextStyle(
                          color: currentTab == 1 ? Colors.blue : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                )

              ],
            ),

            //-------------- ROW RIGHT (TAB BAR ICON) -------------------
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                //................. CREATE BUTTON PROFILE ..............
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentScreen =
                          Profile(); // if user taps on this dashboard tab will be active
                      currentTab = 2;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.person,
                        color: currentTab == 2 ? Colors.blue : Colors.grey,
                      ),
                      Text(
                        'Profile',
                        style: TextStyle(
                          color: currentTab == 2 ? Colors.blue : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                ),

                //.................. CREATE BUTTON SETTINGS ............
                MaterialButton(
                  minWidth: 40,
                  onPressed: () {
                    setState(() {
                      currentScreen =
                          Settings(); // if user taps on this dashboard tab will be active
                      currentTab = 3;
                    });
                  },
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.chat,
                        color: currentTab == 3 ? Colors.blue : Colors.grey,
                      ),
                      Text(
                        'Settings',
                        style: TextStyle(
                          color: currentTab == 3 ? Colors.blue : Colors.grey,
                        ),
                      ),
                    ],
                  ),
                )

              ],
            )

          ],

        ),
      ),
    );
  }
}
//******************************************************************************
